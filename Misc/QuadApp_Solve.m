function [HX,HZ,HXX,HXZ,HZZ,H0,Hvv,GX,GZ,GXX,GXZ,GZZ,G0,Gvv] = ...
    QuadApp_Solve(dGam,d2Gam,N,nx,ny,nz,Omega,Gam0)

% Version 1.0, written by Kerk Phillips, November/December 2013
% Implements the perturbation method found in Heer & Maussner (2007).
%
% This function calculates the linear and quadratic coefficients of the
% approximate policy function(s) and jump functions(s).  The policy
% function is H{X(t01),Z(t),v} and the jump function is G{X(t01),Z(t),v}.
% Single subscripts denote first derivative terms, double subscripts denote
% second derivative terms, and zeros denote constant terms (which will be
% zero if linearizing about the steady state).
%
% This function takes the following inputs:
%  dGam  - (ny+nx)-by-(3nx+2ny+2nz) array of 1st derivatives
%  d2Gam - (ny+nx)-by-(3nx+2ny+2nz)-by-(3nx+2ny+2nz) array of 2nd derivs
%  nx    - is the number of elements in X
%  ny    - is the number of elements in Y
%  nz    - is the number of elements in Z
%  N     - nz-by-nz matrix of autoregressive coefficients
%  Omega - nz-by-nz matrix of correlations for innovations to Z processes
%          which takes the form sig*Omega*eps, eps ~ N(0,I)
%  Gam0  - is the constant from the Taylor-series approximation and will be
%          zero when approximating about the steady state.
% The function generates the following matrices for simulation:
%  HX    - nx-by-nx  matrix of X(t-1) on X(t) coefficients
%  HZ    - nx-by-nx  matrix of Z(t) on X(t) coefficients
%  HXX   - nx-by-nx-by-nx  array of X(t-1)'*X(t-1) on X(t) coefficients
%  HXZ   - nx-by-nz-by-nx  array of X(t-1)'*Z(t) on X(t) coefficients
%  HZZ   - nz-by-nz-by-nx  array of Z(t)'*Z(t) on X(t) coefficients
%  H0    - nx-by-1 vector of constants
%  Hvv   - nx-by-1 vector of coefficients on v^2
%  GX    - ny-by-nx  matrix of X(t-1) on Y(t) coefficients
%  GZ    - ny-by-nx  matrix of Z(t) on Y(t) coefficients
%  GXX   - nx-by-nx-by-ny  array of X(t-1)'*X(t-1) on Y(t) coefficients
%  GXZ   - nx-by-nz-by-ny  array of X(t-1)'*Z(t) on Y(t) coefficients
%  GZZ   - nz-by-nz-by-ny  array of Z(t)'*Z(t) on Y(t) coefficients
%  G0    - ny-by-1 vector of constants
%  Gvv   - ny-by-1 vector of coefficients on v^2
%
% For HXX, HXZ, HZZ, GXX, GXZ & GZZ the 3rd dimension corresponds to the 
% equation number.  The same is true of d2Gam.  For all other 2 and 1 
% dimensional matrices the rows correspond to equations.
%
%  dGam - is an (nx+ny)-by-(3nx+2ny+2nz) matrix of first derivatives, 
%   with the rows corresponding to equations in the function and the 
%   columns corresponding to the 3nx+2ny+2nz elements of theta.
%   There are seven sets of variables with the following designations:
%   Set 1 is X(t+1)
%   Set 2 is X(t)
%   Set 3 is X(t-1)
%   Set 4 is Y(t+1)
%   Set 5 is Y(t)
%   Set 6 is Z(t+1)
%   Set 7 is Z(t)
%   where X is the set of endogenous state variables,
%   Y is the set of "jump", costate or control variables,
%   and Z is the set of exogenous state variable.
%
%  d2Gam - is an (3nx+2ny+2nz)-by-(3nx+2ny+2nz)-by-(nx+ny) array or 
%   tensor of second derivatives, with the rows corresponding to 
%   equations in the function and the other two dimensions corresponding 
%   to the 3nx+2ny+2nz elements of theta.

% Find the linear terms using Uhlig's solver
% leng = 3*nx+2*nz;
NN = N;
GamY0 = Gam0(1:ny,:);
GamY1 = dGam(1:ny,1:nx);
GamY2 = dGam(1:ny,nx+1:2*nx);                      %Uhlig's A
% GamY3 = dGam(1:ny,2*nx+1:3*nx);                  %Uhlig's B
% GamY4 = dGam(1:ny,3*nx+1:3*nx+ny);
GamY5 = dGam(1:ny,3*nx+ny+1:3*nx+2*ny);            %Uhlig's C
% GamY6 = dGam(1:ny,3*nx+2*ny1:3*nx+2*ny+nz);
% GamY7 = dGam(1:ny,3*nx+2*ny+nz+1:leng);          %Uhlig's D
GamX0 = Gam0(ny+1:ny+nx,:);
GamX1 = dGam(ny+1:ny+nx,1:nx);                     %Uhlig's F
GamX2 = dGam(ny+1:ny+nx,nx+1:2*nx);                %Uhlig's G
% GamX3 = dGam(ny+1:ny+nx,2*nx+1:3*nx);            %Uhlig's H
GamX4 = dGam(ny+1:ny+nx,3*nx+1:3*nx+ny);           %Uhlig's J
GamX5 = dGam(ny+1:ny+nx,3*nx+ny+1:3*nx+2*ny);      %Uhlig's K
% GamX6 = dGam(ny+1:ny+nx,3*nx+2*ny1:3*nx+2*ny+nz);%Uhlig's L
% GamX7 = dGam(ny+1:ny+nx,3*nx+2*ny+nz+1:leng);    %Uhlig's M\
[HX, HZ, GX, GZ] = UhligSolve(dGam,NN,nx,ny,nz);
H0 = -(GamX1*HX+GamX1+HZ)\GamX0;
if ny>0
    G0 = -(GamY1*GX+GamY1+GZ)\GamY0;
else
    G0 = zeros(0,1);
end

% Find the quadratic terms
% %  Initialize Arrays
% HXX = zeros(nx,nx,nx);
% HXZ = zeros(nx,nz,nx);
% HZZ = zeros(nz,nz,nx);
% Hvv = zeros(nx,1);
% GXX = zeros(nx,nx,ny);
% GXZ = zeros(nx,nz,ny);
% GZZ = zeros(nz,nz,ny);
% Gvv = zeros(ny,1);

% Define lagged coefficient matrices
LX = HX*HX;
LZ = HX*HZ + HZ*N;
MX = GX*HX;
MZ = GX*HZ + GZ*N;

% Create F matrix
F = [LX LZ zeros(nx,1);
     HX           HZ           zeros(nx,1);
     eye(nx)      zeros(nx,nx) zeros(nx,1);
     MX           MZ           zeros(ny,1);
     GX           GZ           zeros(ny,1);
     zeros(nz,nx) N            zeros(nz,1);
     zeros(nz,nx) eye(nz)      zeros(nz,1)];
 
% Create Phi array
Phi = Dmult2post(d2Gam,F);
Phi = Dmult3post(Phi,F);

% Create Phi partitions
PhiYXX = Phi(1:ny,1:nx,1:nx);
PhiYXZ = Phi(1:ny,1:nx,nx+1:nx+nz);
PhiYZZ = Phi(1:ny,nx+1:nx+nz,nx+1:nx+nz);
PhiYvv = Phi(1:ny,nx+nz+1,nx+nz+1);
PhiXXX = Phi(ny+1:ny+nx,1:nx,1:nx);
PhiXXZ = Phi(ny+1:ny+nx,1:nx,nx+1:nx+nz);
PhiXZZ = Phi(ny+1:ny+nx,nx+1:nx+nz,nx+1:nx+nz);
PhiXvv = Phi(ny+1:ny+nx,nx+nz+1,nx+nz+1);

% Solve for HXX and GXX
%  Initial guess for HXX
guess = zeros(nx*.5*(nx+1)*nx,1);
%  Anonymous function to pass parameters
fXX = @(y) DXX(y,nx,PhiYXX,PhiXXX,GamY2,GamY5,GamX1,GamX2,GamX4,GamX5,...
    HX,GX);
HXXvect = fsolve(fXX,guess);
% Reconstruct HXX from the vectorzed input
HXX = VtoSym3(HXXvect,nx,nx);
% Construct GXX values
if ny>0
    GXX = Dmult2pre( -(GamY5)^(-1) , PhiYXX(:,:,:) ) ...
        + Dmult2pre( -(GamY5)^(-1)*GamY2 , HXX(:,:,:) );
else
    GXX = zeros(0,nx,nx);
end

% Solve for HXZ and GXZ
%  Initial guess for HXZ
guess = zeros(nx*nx*nz,1);
%  Anonymous function to pass parameters
fXZ = @(y) DXZ(y,nx,PhiYXZ,PhiXXZ,GamY2,GamY5,GamX1,GamX2,GamX4,GamX5,...
    HX,GX,HZ,N,HXX,GXX);
HXZvect = fsolve(fXZ,guess);
% Reconstruct HXZ from the vectorzed input
HXZ = VtoRec3(HXZvect,nx,nx,nz);
% Construct GXZ values
if ny>0
    GXZ = Dmult2pre( -(GamY5)^(-1) , PhiYXZ(:,:,:) ) ...
        + Dmult2pre( -(GamY5)^(-1)*GamY2 , HXZ(:,:,:) );
else
    GXZ = zeros(0,nx,nz);
end

% Solve for HZZ and GZZ
%  Initial guess for HZZ
guess = zeros(nx*.5*(nz+1)*nz,1);
%  Anonymous function to pass parameters
fZZ = @(y) DZZ(y,nx,PhiYZZ,PhiXZZ,GamY2,GamY5,GamX1,GamX2,GamX4,GamX5,...
    HX,GX,HZ,N,HXX,GXX,HXZ,GXZ);
HZZvect = fsolve(fZZ,guess);
% Reconstruct HZZ from the vectorzed input
HZZ = VtoSym3(HZZvect,nx,nz);
% Construct GZZ values
if ny>0
    GZZ = Dmult2pre( -(GamY5)^(-1) , PhiYZZ(:,:,:) ) ...
        + Dmult2pre( -(GamY5)^(-1)*GamY2 , HZZ(:,:,:) );
else
    GZZ = zeros(0,nz,nz);
end

% Solve for Hvv
Hvv = Dvv(nx,PhiYvv,PhiXvv,GamY2,GamY5,GamX1,GamX2,GamX4,GamX5,...
    HX,GX,HZZ,GZZ,Omega);
% Construct Gvv values
if ny>0
    Gvv = Dmult2pre( -(GamY5)^(-1) , PhiYvv(:,:,:) ) ...
        + Dmult2pre( -(GamY5)^(-1)*GamY2 , Hvv(:,:,:) );
else
    Gvv = zeros(0,1,1);
end

end


function DeltaXXvect = DXX(HXXvect,nx,PhiYXX,PhiXXX,GamY2,GamY5, ...
    GamX1,GamX2,GamX4,GamX5,HX,GX)

% Version 1.0, written by Kerk Phillips, November/December 2013
% Creates the XX deviations, DeltaXX = PhiXX - dGam x dF as explained in 
% the documentation.
%
% This program takes the vectorization of an nx-by-nx-by-nx input array,
% HXX.  HXX is symmetric in the 2nd and 3rd dimensions, so that 
% HXX(i,j,k) = HXX(i,k,j)
%
% It output is the vectorization of an nx-by-nx-by-nx array, DeltaXX, that
% is also symmetric in the 2nd and 3rd dimensions.

[ny,~,~] = size(PhiYXX);

% Reconstruct HXX from the vectorzed input
HXX = VtoSym3(HXXvect,nx,nx);

% Construct GXX values
if ny > 0
    GXX = Dmult2pre( -(GamY5)^(-1) , PhiYXX ) ...
        + Dmult2pre( -(GamY5)^(-1)*GamY2 , HXX );
end
    
% Calculate DeltaXX array
if ny >0
    DeltaXX = PhiXXX ...
        + Dmult2pre(GamX1,Dmult2post(Dmult3post(HXX,HX),HX)) ...
        + Dmult2pre(GamX1*HX,HXX) ...
        + Dmult2pre(GamX2,HXX) ...
        + Dmult2pre(GamX4,Dmult2post(Dmult3post(GXX,HX),HX)) ...
        + Dmult2pre(GamX4*GX,HXX) ...
        + Dmult2pre(GamX5,GXX);
else
    DeltaXX = PhiXXX ...
        + Dmult2pre(GamX1,Dmult2post(Dmult3post(HXX,HX),HX))...
        + Dmult2pre(GamX1*HX,HXX) - Dmult2pre(GamX2,HXX);
end

% Vectorize DeltaXX
DeltaXXvect = Sym3toV(DeltaXX,nx,nx);

end


function DeltaXZvect = DXZ(HXZvect,nx,PhiYXZ,PhiXXZ,GamY2,GamY5, ...
    GamX1,GamX2,GamX4,GamX5,HX,GX,HZ,N,HXX,GXX)

% Version 1.0, written by Kerk Phillips, November/December 2013
% Creates the XX deviations, DeltaXX = PhiXX - dGam x dF as explained in 
% the documentation.
%
% This program takes the vectorization of an nx-by-nx-by-nx input array,
% HXX.  HXX is symmetric in the 2nd and 3rd dimensions, so that 
% HXX(i,j,k) = HXX(i,k,j)
%
% It output is the vectorization of an nx-by-nx-by-nx array, DeltaXX, that
% is also symmetric in the 2nd and 3rd dimensions.

[ny,~,~] = size(PhiYXZ);
[~,~,nz] = size(PhiXXZ);

% Reconstruct HXZ from the vectorzed input
HXZ = VtoRec3(HXZvect,nx,nx,nz);

% Construct GXZ values
if ny > 0
    GXZ = Dmult2pre( -(GamY5)^(-1) , PhiYXZ ) ...
        + Dmult2pre( -(GamY5)^(-1)*GamY2 , HXZ );
end
    
% Calculate DeltaXX array
if ny > 0
    DeltaXZ = PhiXXZ ...
        + Dmult2pre(GamX1,Dmult2post(Dmult3post(HXX,HZ),HX)) ...
        + Dmult2pre(GamX1,Dmult2post(Dmult3post(HXZ,N),HX)) ...
        + Dmult2pre(GamX1*HX,HXZ) ... 
        + Dmult2pre(GamX2,HXZ) ...
        + Dmult2pre(GamX4,Dmult2post(Dmult3post(GXX,HZ),HX)) ...
        + Dmult2pre(GamX4,Dmult2post(Dmult3post(GXX,N),HX)) ...
        + Dmult2pre(GamX4*GX,HXZ) ...
        + Dmult2pre(GamX5,GXZ);
else
    DeltaXZ = PhiXXZ ...
        + Dmult2pre(GamX1,Dmult2post(Dmult3post(HXX,HZ),HX)) ...
        + Dmult2pre(GamX1,Dmult2post(Dmult3post(HXZ,N),HX)) ...
        + Dmult2pre(GamX1*HX,HXZ) ... 
        + Dmult2pre(GamX2,HXZ);
end

% Vectorize DeltaXX
DeltaXZvect = Rec3toV(DeltaXZ,nx,nx,nz);

end


function DeltaZZvect = DZZ(HZZvect,nx,PhiYZZ,PhiXZZ,GamY2,GamY5, ...
    GamX1,GamX2,GamX4,GamX5,HX,GX,HZ,N,HXX,GXX,HXZ,GXZ)

% Version 1.0, written by Kerk Phillips, November/December 2013
% Creates the XX deviations, DeltaXX = PhiXX - dGam x dF as explained in 
% the documentation.
%
% This program takes the vectorization of an nx-by-nx-by-nx input array,
% HXX.  HXX is symmetric in the 2nd and 3rd dimensions, so that 
% HXX(i,j,k) = HXX(i,k,j)
%
% It output is the vectorization of an nx-by-nx-by-nx array, DeltaXX, that
% is also symmetric in the 2nd and 3rd dimensions.

[ny,~,~] = size(PhiYZZ);
[~,~,nz] = size(PhiXZZ);

% Reconstruct HXX from the vectorzed input
HZZ = VtoSym3(HZZvect,nx,nz);

% Construct HZX fro HXZ
HZX = permute(HXZ, [1 3 2]);

% Construct GZX fro GXZ
GZX = permute(GXZ, [1 3 2]);

% Construct GXX values
if ny > 0
    GZZ = Dmult2pre( -(GamY5)^(-1) , PhiYZZ ) ...
        + Dmult2pre( -(GamY5)^(-1)*GamY2 , HZZ );
end
    
% Calculate DeltaXX array
if ny >0
    DeltaZZ = PhiXZZ ...
        + Dmult2pre(GamX1,Dmult2post(Dmult3post(HXX,HZ),HX)) ...
        + Dmult2pre(GamX1,Dmult2post(Dmult3post(HXZ,N),HX)) ...
        + Dmult2pre(GamX1*HX,HZZ) ... 
        + Dmult2pre(GamX1,Dmult2post(Dmult3post(HZX,HZ),N)) ...
        + Dmult2pre(GamX1,Dmult2post(Dmult3post(HZZ,N),N)) ...
        + Dmult2pre(GamX2,HZZ) ...
        + Dmult2pre(GamX4,Dmult2post(Dmult3post(GXX,HZ),HX)) ...
        + Dmult2pre(GamX4,Dmult2post(Dmult3post(GXZ,N),HX)) ...
        + Dmult2pre(GamX4*GX,HZZ) ... 
        + Dmult2pre(GamX4,Dmult2post(Dmult3post(GZX,HZ),N)) ...
        + Dmult2pre(GamX4,Dmult2post(Dmult3post(GZZ,N),N)) ...
        + Dmult2pre(GamX5,GZZ);
else
    DeltaZZ = PhiXZZ ...
        + Dmult2pre(GamX1,Dmult2post(Dmult3post(HXX,HZ),HX)) ...
        + Dmult2pre(GamX1,Dmult2post(Dmult3post(HXZ,N),HX)) ...
        + Dmult2pre(GamX1*HX,HZZ) ... 
        + Dmult2pre(GamX1,Dmult2post(Dmult3post(HZX,HZ),N)) ...
        + Dmult2pre(GamX1,Dmult2post(Dmult3post(HZZ,N),N)) ...
        + Dmult2pre(GamX2,HZZ);
end

% Vectorize DeltaXX
DeltaZZvect = Sym3toV(DeltaZZ,nx,nz);

end


function Hvv = Dvv(nx,PhiYvv,PhiXvv,GamY2,GamY5, ...
    GamX1,GamX2,GamX4,GamX5,HX,GX,HZZ,GZZ,Omega)

% Version 1.0, written by Kerk Phillips, November/December 2013
% Creates the XX deviations, DeltaXX = PhiXX - dGam x dF as explained in 
% the documentation.
%
% This program takes the vectorization of an nx-by-nx-by-nx input array,
% HXX.  HXX is symmetric in the 2nd and 3rd dimensions, so that 
% HXX(i,j,k) = HXX(i,k,j)
%
% It output is the vectorization of an nx-by-nx-by-nx array, DeltaXX, that
% is also symmetric in the 2nd and 3rd dimensions.

[ny,~,~] = size(PhiYvv);

% Calculate trace vectors
traceX = zeros(nx,1);
tempX = permute(Dmult2post(Dmult3post(HZZ,Omega),Omega),[2 3 1]);
for i=1:nx
    traceX(i) = trace(tempX(:,:,i));
end

if ny>0
    traceY = zeros(ny,1);
    tempY = permute(Dmult2post(Dmult3post(GZZ,Omega),Omega),[2 3 1]);
    for i=1:ny
        traceY(i) = trace(tempY(:,:,i));
    end
end
    

% Calculate DeltaXX array
if ny >0
    Preterm = GamX1*HX + GamX1 + GamX2 + GamX4*GX ...
        + GamX4*(GamY5)^(-1)*GamY2 ...
        + GamX5*(GamY5)^(-1)*GamY2;
    Postterm = PhiXvv + GamX1*traceX + GamX4*traceY ...
        - (GamX5+GamX5)*(GamY5)^(-1)*PhiYvv;
else
    Preterm = GamX1*HX + GamX1 + GamX2;
    Postterm = PhiXvv + GamX1*traceX;
end

Hvv = Preterm \ Postterm;

end


function vect = Rec3toV(Rec,numr,numc,numd)

% Version 1.0, written by Kerk Phillips, December 2013
%
% This function converts a 3-dimensional array with numr rows and numc 
%columns and slices, that is symmetric in the 2nd and 3rd dimensions,
% so that Sym(i,j,k) = Sym(i,k,j), to a vector.

[nr,nc,nd] = size(Rec);

if nr ~= numr
    disp('wrong number of rows in input')
elseif nc ~= numc
    disp('wrong number of columns in input')
elseif nd ~= numd
    disp('wrong number of slices in input')
end

vect = zeros(numr*numc*numd,1);

n = 1;
for i = 1:numr
    for j = 1:numc
        for k = 1:numd
            vect(n)= Rec(i,j,k) ;
            n = n+1;
        end
    end
end

end


function vect = Sym3toV(Sym,numr,numc)

% Version 1.0, written by Kerk Phillips, December 2013
%
% This function converts a 3-dimensional array with numr rows and numc 
%columns and slices, that is symmetric in the 2nd and 3rd dimensions,
% so that Sym(i,j,k) = Sym(i,k,j), to a vector.

[nr,nc,nd] = size(Sym);

if nr ~= numr
    disp('wrong number of rows in input')
elseif nc ~= numc
    disp('wrong number of columns in input')
elseif nd ~= numc
    disp('wrong number of slices in input')
end

vect = zeros(numr*numc^2,1);

n = 1;
for i = 1:numr
    for j = 1:numc
        for k = j:numc
            if Sym(i,j,k) == Sym(i,k,j)
                vect(n)= Sym(i,j,k) ;
            else
                disp('input matrix is not appropriately symmetric')
            end
            n = n+1;
        end
    end
end

end


function Rec = VtoRec3(vect,numr,numc,numd)

% Version 1.0, written by Kerk Phillips, December 2013
%
% This function converts a vector to a 3-dimensional array with numr rows
% and numc columns and numd slices.

[nr,nc] = size(vect);

if nc>1 && nr>1
    disp('input must be a vector')
elseif nc>1 && nr==1
    vect = vect';
end

if nr ~= numr*numc*numd
    disp('wrong number of elements in input')
end

Rec = zeros(numr,numc,numd);

n = 1;
for i = 1:numr
    for j = 1:numc
        for k = 1:numd
            Rec(i,j,k) = vect(n);
            n = n+1;
        end
    end
end

end


function Sym = VtoSym3(vect,numr,numc)

% Version 1.0, written by Kerk Phillips, December 2013
%
% This function converts a vector to a 3-dimensional array with numr rows
% and numc columns and slices, that is symmetric in the 2nd and 3rd 
% dimensions, so that Sym(i,j,k) = Sym(i,k,j).

[nr,nc] = size(vect);

if nc>1 && nr>1
    disp('input must be a vector')
elseif nc>1 && nr==1
    vect = vect';
end

if nr ~= numr*numc^2
    disp('wrong number of elements in input')
end

Sym = zeros(numr,numc,numc);

n = 1;
for i = 1:numr
    for j = 1:numc
        for k = j:numc
            Sym(i,j,k) = vect(n);
            if k>j
                Sym(i,k,j) = vect(n);
            end
            n = n+1;
        end
    end
end

end


function A = Dmult2post(B,C)
% Version 1.0, written by Kerk Phillips, November 2013
%
% This function multiplies a 3-dimensional array, B, with a two-dimensional
% matrix C along the second dimension, slice-by-slice

[rB,cB,dB] = size(B);
[rC,cC,dC] = size(C);


if dC > 1 % problems if C is 3-dimensional
    disp('C has deapth and should be 2-dimensional')
elseif cB ~= rC % B and C not conformable along 2nd dimension
    disp('B and C are not conformable along the 2nd dimension')
else
    A = zeros(rB,cC,dB);
    for j = 1:dB
        A(:,:,j) = B(:,:,j)*C;
    end
end


end


function A = Dmult2pre(B,C)
% Version 1.0, written by Kerk Phillips, December 2013
%
% This function multiplies a 3-dimensional array, B, with a two-dimensional
% matrix C along the second dimension, slice-by-slice

[rB,cB,dB] = size(B);
[rC,cC,dC] = size(C);


if dC > 1 % problems if C is 3-dimensional
    disp('C has deapth and should be 2-dimensional')
elseif cB ~= rC % B and C not conformable along 2nd dimension
    disp('B and C are not conformable along the 2nd dimension')
else
    A = zeros(rB,cC,dB);
    for j = 1:dB
        A(:,:,j) = C*B(:,:,j);
    end
end


end


function A = Dmult3post(B,C)
% Version 1.0, written by Kerk Phillips, December 2013
%
% This function multiplies a 3-dimensional array, B, with a two-dimensional
% matrix C along the third dimension, slice-by-slice

B2 = permute(B,[1 3 2]);
A2 = Dmult2post(B2,C);
A = permute(A2,[1 3 2]);

end


function [PP, QQ, RR, SS] = UhligSolve(DerivMat1,NN,nx,ny,nz)
% Written by Kerk Phillips, November 2013
% Version 1.1 updated April 1, 2014
% This code takes Uhlig's original code and puts it in the form of a
% function.  This version outputs the poloicy function coefficients: PP,
% QQ,RR and SS.  It takes the matrix of deriviatives: DerivMat1 and NN as 
% inputs.
% nx, ny & nz are the number of variables of each type.
% DO_QZ is a binary indicator telling the program to use the QZ method if
% true.
% Z0 is the z-point about which the linearization is taken.  For
% linearizing about the steady state this is Zbar and normally Zbar=0.

leng = 3*nx+2*ny+2*nz;
AA = DerivMat1(1:ny,nx+1:2*nx);
BB = DerivMat1(1:ny,2*nx+1:3*nx);
CC = DerivMat1(1:ny,3*nx+ny+1:3*nx+2*ny);
DD = DerivMat1(1:ny,3*nx+2*ny+nz+1:leng);
FF = DerivMat1(ny+1:ny+nx,1:nx);
GG = DerivMat1(ny+1:ny+nx,nx+1:2*nx);
HH = DerivMat1(ny+1:ny+nx,2*nx+1:3*nx);
JJ = DerivMat1(ny+1:ny+nx,3*nx+1:3*nx+ny);
KK = DerivMat1(ny+1:ny+nx,3*nx+ny+1:3*nx+2*ny);
LL = DerivMat1(ny+1:ny+nx,3*nx+2*ny+1:3*nx+2*ny+nz);
MM = DerivMat1(ny+1:ny+nx,3*nx+2*ny+nz+1:leng);


message = '                                                                       ';
warnings = [];
DISPLAY_IMMEDIATELY = 1;
TOL = .000001; % Roots smaller than TOL are regarded as zero.
               % Complex numbers with distance less than TOL 
               % are regarded as equal.
if exist('MANUAL_ROOTS')~=1,
   MANUAL_ROOTS = 0; % = 1, if you want to choose your own
                               % roots, otherwise set = 0. 
                               % See SOLVE.M for instructions.
end
if exist('IGNORE_VV_SING')~=1,
   IGNORE_VV_SING = 1; % =1: Ignores, if VV is singular.  
                       % Sometimes useful for sunspots.  
                       % Cross your fingers...  
end
DISPLAY_ROOTS = 0;  % Set = 1, if you want to see the roots.

% VERSION 2.0, MARCH 1997, COPYRIGHT H. UHLIG.
% SOLVE.M solves for the decision rules in a linear system,
% which is assumed to be of the form
% 0 = AA x(t) + BB x(t-1) + CC y(t) + DD z(t)
% 0 = E_t [ FF x(t+1) + GG x(t) + HH x(t-1) + JJ y(t+1) + KK y(t) +...
%   LL z(t+1) + MM z(t)]
% z(t+1) = NN z(t) + epsilon(t+1) with E_t [ epsilon(t+1) ] = 0,
% where it is assumed that x(t) is the endogenous state vector,
% y(t) the other endogenous variables and z(t) the exogenous state
% vector.  It is assumed that the row dimension of AA is at least as large
% as the dimensionality of the endogenous state vector x(t).  
% The program solves for the equilibrium law of motion
% x(t) = PP x(t-1) + QQ z(t)
% y(t) = RR x(t-1) + SS z(t).
% To use this program, define the matrices AA, BB, .., NN.
% SOLVE.M then calculates PP, QQ, RR and SS.  It also calculates
% WW with the property [x(t)',y(t)',z(t)']=WW [x(t)',z(t)'].
% 
% A few additional variables are used
% overwriting variables with the same names
% that might have been used before.  They are:
% sumimag, sumabs, message, warnings, CC_plus, CC_0, Psi_mat, Gamma_mat,
% Theta_mat, Xi_mat, Delta_mat, Xi_eigvec, Xi_eigval, Xi_sortabs, 
% Xi_sortindex, Xi_sortvec, Xi_sortval, Xi_select, drop_index, Omega_mat,
% Lambda_mat, PP_imag, VV, LLNN_plus_MM, QQSS_vec
% 
% Source: H. Uhlig (1995) "A Toolkit for Solving Nonlinear Dynamic
% Stochastic Models Easily," Discussion Paper, Institute for
% Empirical Macroeconomis, Federal Reserve Bank of Minneapolis #101 or
% Tilburg University, CentER DP 9597.
%
% This update includes the suggestion by Andrew Atkeson to use generalized
% eigenvalues to perform the computations.  IN PARTICULAR, THE DEFINITION
% OF PSI_MAT, GAMMA_MAT and THETA_MAT have been changed!
%
% You can also select roots manually.  For the manual selection procedure,
% see the instructions upon inspecting this file (filename: solve.m)

% Copyright: H. Uhlig.  Feel free to copy, modify and use at your own risk.
% However, you are not allowed to sell this software or otherwise impinge
% on its free distribution.

      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      %  MANUAL SELECTION OF ROOTS PROCEDURE.  INSTRUCTIONS:     %
      % For manual selection, set MANUAL_ROOTS = 1 and %
      % define Xi_manual somewhere earlier in your calculations. %
      % Xi_manual should be a vector of length m_states with     %
      % distinct integer entries between 1 and 2*m_states. The   %
      % program then uses the roots Xi_sortval(Xi_manual) and    %
      % the corresponding eigenvectors Xi_sortvec(Xi_manual).    %
      % Thus, to choose the desired roots, run the program once  %
      % with automatic root selection, take a look at Xi_sortval,%
      % and Xi_sortvec and write down the indices of the desired %  
      % roots.                                                   %
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[~,m_states] = size(AA);
[l_equ,n_endog ] = size(CC);
k_exog = min(size(NN));
sumimag = sum(sum(abs(imag(AA))))+sum(sum(abs(imag(BB))))...
    +sum(sum(abs(imag(CC))));
sumabs  = sum(sum(abs(AA)))   +sum(sum(abs(BB)))   +sum(sum(abs(CC)));
if sumimag / sumabs > .000001,
  message = ...
['SOLVE.M: I hate to point this out to you, but some of your matrices    '  
'         contain complex numbers, which does not make much sense. You  '
'         should check your steady state parameters and calculations.   '
'         I will proceed anyhow, but you will probably get nonsense.    '];
  if DISPLAY_IMMEDIATELY, disp(message); end;
  warnings = [warnings;message];
end
if rank(CC)<n_endog,
  message = ...
'SOLVE.M: Sorry!  Rank(CC) needs to be at least n! Cannot solve for PP. ';
  if DISPLAY_IMMEDIATELY, disp(message); end;
  warnings = [warnings;message];
else
  CC_plus = pinv(CC);
  CC_0 = (null(CC'))';
  Psi_mat   = [ zeros(l_equ-n_endog,m_states)
              FF - JJ*CC_plus*AA           ];
  Gamma_mat = [ CC_0 * AA
                JJ*CC_plus*BB - GG + KK*CC_plus*AA ];
  Theta_mat = [ CC_0 * BB
                KK*CC_plus*BB - HH                 ];
  Xi_mat    = [ Gamma_mat,     Theta_mat
                eye(m_states), zeros(m_states) ];
  Delta_mat = [ Psi_mat,       zeros(m_states)
                zeros(m_states), eye(m_states) ];
  [Xi_eigvec,Xi_eigval] = eig(Xi_mat,Delta_mat);
  if rank(Xi_eigvec)<m_states,
     message = ...
 'SOLVE.M: Sorry! Xi is not diagonalizable! Cannot solve for PP.         ';
     if DISPLAY_IMMEDIATELY, disp(message); end;
     warnings = [warnings;message];
  else
    [Xi_sortabs,Xi_sortindex] = sort(abs(diag(Xi_eigval)));
    Xi_sortvec = Xi_eigvec(1:2*m_states,Xi_sortindex);
    Xi_sortval = diag(Xi_eigval(Xi_sortindex,Xi_sortindex));
    Xi_select = 1 : m_states;
    if imag(Xi_sortval(m_states))~=0,
      if (abs( Xi_sortval(m_states) - conj(Xi_sortval(m_states+1)) )...
              < TOL),
      % NOTE: THIS LAST LINE MIGHT CREATE PROBLEMS, IF THIS EIGENVALUE 
      % OCCURS MORE THAN ONCE!!
      % IF YOU HAVE THAT PROBLEM, PLEASE TRY MANUAL ROOT SELECTION.  
        drop_index = 1;
        while (abs(imag(Xi_sortval(drop_index)))>TOL) && ...
                (drop_index < m_states),
          drop_index = drop_index + 1;
        end
        if drop_index >= m_states,
          message = ...
['SOLVE.M: You are in trouble. You have complex eigenvalues, and I cannot'
'   find a real eigenvalue to drop to only have conjugate-complex pairs.'
'   Put differently: your PP matrix will contain complex numbers. Sorry!'];
          if DISPLAY_IMMEDIATELY, disp(message); end;
          warnings = [warnings;message];
          if m_states == 1,
            message = ...
['   TRY INCREASING THE DIMENSION OF YOUR STATE SPACE BY ONE!            '
'   WATCH SUNSPOTS!                                                     '];                     
            if DISPLAY_IMMEDIATELY, disp(message); end;
            warnings = [warnings;message];
          end
        else
          message = ...
['SOLVE.M: I will drop the lowest real eigenvalue to get real PP.        '
'         I hope that is ok. You may have sunspots.                     ']; 
          if DISPLAY_IMMEDIATELY, disp(message); end;
          warnings = [warnings;message];
          Xi_select = [ 1: (drop_index-1), (drop_index+1):(m_states+1)];
        end
      end
    end
    if MANUAL_ROOTS,
      message = ...
['SOLVE.M: You have chosen to select roots manually.  I am crossing my   '
'         fingers that you are doing it correctly.  In particular,      '
'         you should have defined Xi_manual.  Type help solve           '
'         and inspect SOLVE.M to get further information on how to do it'];
      if DISPLAY_IMMEDIATELY, disp(message); end;
      warnings = [warnings;message];
      if exist('Xi_manual'),
         Xi_select = Xi_manual;
      else
         message = ...
['SOLVE.M: You have not defined Xi_manual.  Either define it or turn off '
'         the manual roots selection procedure with                     '
'         MANUAL_ROOTS = 0                                              '
'         Right now, I better let your calculations crash - sorry!      '
'         If you get results, they are based on previous calculations.  '];
         disp(message);
         warnings = [warnings;message];
      end
    else
      if max(Xi_select) < 2*m_states,
        if Xi_sortabs(max(Xi_select)+1) < 1 - TOL,
          message = ...
['SOLVE.M: You may be in trouble. There are stable roots NOT used for PP.'
'         I have used the smallest roots: I hope that is ok.            '  
'         If not, try manually selecting your favourite roots.          '
'         For manual root selection, take a look at the file solve.m    '
'         Watch out for sunspot solutions.                              '];
          if DISPLAY_IMMEDIATELY, disp(message); end;
          warnings = [warnings;message];
        end 
      end
    if max(abs(Xi_sortval(Xi_select)))  > 1 + TOL,
      message = ...
['SOLVE.M: You may be in trouble.  There are unstable roots used for PP. '
'         Keep your fingers crossed or change your model.               '];
      if DISPLAY_IMMEDIATELY, disp(message); end;
      warnings = [warnings;message];
    end
    if abs( max(abs(Xi_sortval(Xi_select))) - 1  ) < TOL,
      message = ...
['SOLVE.M: Your matrix PP contains a unit root. You probably do not have '
'         a unique steady state, do you?  Should not be a problem, but  '
'         you do not have convergence back to steady state after a shock'
'         and you should better not trust long simulations.             '];
      if DISPLAY_IMMEDIATELY, disp(message); end;
      warnings = [warnings;message];
    end

    Lambda_mat = diag(Xi_sortval(Xi_select));
    Omega_mat  = [Xi_sortvec((m_states+1):(2*m_states),Xi_select)];
    if rank(Omega_mat)<m_states,
      message = ...
'SOLVE.M: Sorry! Omega is not invertible. Cannot solve for PP.          ';
      if DISPLAY_IMMEDIATELY, disp(message); end;
      warnings = [warnings;message];
    else
      PP = Omega_mat*Lambda_mat/Omega_mat;
      PP_imag = imag(PP);
      PP = real(PP);
      if sum(sum(abs(PP_imag))) / sum(sum(abs(PP))) > .000001,
        message = ...
['SOLVE.M: PP is complex.  I proceed with the real part only.            '  
'         Hope that is ok, but you are probably really in trouble!!     '
'         You should better check everything carefully and be           '
'         distrustful of all results which follow now.                  '];
        if DISPLAY_IMMEDIATELY, disp(message); end;
        warnings = [warnings;message];
      end
      RR = - CC_plus*(AA*PP+BB);
      VV = [ kron(eye(k_exog),AA),   kron(eye(k_exog),CC)
             kron(NN',FF)+kron(eye(k_exog),(FF*PP+JJ*RR+GG)), ...
                                     kron(NN',JJ)+kron(eye(k_exog),KK) ];
      if ( (rank(VV) < k_exog*(m_states+n_endog)) && ...
                                       (~IGNORE_VV_SING) ),
        message = ...
['SOLVE.M: Sorry! V is not invertible.  Cannot solve for QQ and SS. You  '
'         can try setting IGNORE_VV_SING = 1 and wish for the best...   '];
        if DISPLAY_IMMEDIATELY, disp(message); end;
        warnings = [warnings;message];
      else
        if ( (rank(VV) < k_exog*(m_states+n_endog)) ),
          message = ...
['SOLVE.M: Warning! V is not invertible.  However, you have set          '
'         IGNORE_VV_SING = 1, and thus, since you have told me to       '
'         ignore this, I will proceed.  Keep your fingers crossed...    ']
          if DISPLAY_IMMEDIATELY, disp(message); end;
          warnings = [warnings;message];
        end
        LLNN_plus_MM = LL*NN + MM;
        DD(:)
        LLNN_plus_MM(:)
        QQSS_vec = - VV \ [ DD(:)
                            LLNN_plus_MM(:) ];
        if max(abs(QQSS_vec)) == Inf,
           message = ...
['SOLVE.M: You probably are in trouble!  QQ or SS contain undefined      '
'         entries! Most likely, the matrix VV is not invertible.        '];
           if DISPLAY_IMMEDIATELY, disp(message); end;
           warnings = [warnings;message];
        end           
        QQ = reshape(QQSS_vec(1:m_states*k_exog),m_states,k_exog);
        SS = reshape(QQSS_vec((m_states*k_exog+1):...
            ((m_states+n_endog)*k_exog)),n_endog,k_exog);
        WW = [ eye(m_states)         , zeros(m_states,k_exog)
               RR*pinv(PP)           , (SS-RR*pinv(PP)*QQ) 
               zeros(k_exog,m_states), eye(k_exog)            ];
      end
    end
    end
  end

end

end
