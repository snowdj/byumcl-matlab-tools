function X = QuadApp_Sim(Xm,Z,v,Pord,HX,HZ,HXX,HXZ,HZZ,H0,Hvv, ...
    GX,GZ,GXX,GXZ,GZZ,G0,Gvv)

% Version 1.0, written by Kerk Phillips, November 2013
% Uses the coeffiecients from a linear or quadratic approximation to
% generate data for next period given today's state.  The set of endogenous
% state varaibles known today is Xm and the set of exogenous state
% variables is Z.  This program generates X.  The input and output values
% are in deviation from the linearization point (almost always the steady
% state).  This means you will need to add back the steady state values
% after you have called this function.  How you do this depends on whether
% you used log-linarization or simple linearization in deriving the values
% of the input coefficients.
%
% This function takes the following inputs:
%  HX    - nx-by-nx  matrix of X(t-1) on X(t) coefficients
%  HZ    - nx-by-nx  matrix of Z(t) on X(t) coefficients
%  HXX   - nx-by-nx-by-nx  array of X(t-1)'*X(t-1) on X(t) coefficients
%  HXZ   - nx-by-nz-by-nx  array of X(t-1)'*Z(t) on X(t) coefficients
%  HZZ   - nz-by-nz-by-nx  array of Z(t)'*Z(t) on X(t) coefficients
%  H0    - nx-by-1 vector of constants
%  Hvv   - nx-by-1 vector of coefficients on v^2
%  GX    - ny-by-nx  matrix of X(t-1) on Y(t) coefficients
%  GZ    - ny-by-nx  matrix of Z(t) on Y(t) coefficients
%  GXX   - nx-by-nx-by-ny  array of X(t-1)'*X(t-1) on Y(t) coefficients
%  GXZ   - nx-by-nz-by-ny  array of X(t-1)'*Z(t) on Y(t) coefficients
%  GZZ   - nz-by-nz-by-ny  array of Z(t)'*Z(t) on Y(t) coefficients
%  G0    - ny-by-1 vector of constants
%  Gvv   - ny-by-1 vector of coefficients on v^2
%  Porf  - is the order of approximation to use, 1 is linear, 2 is
%          quadratic and the default.
% For HXX, HXZ & HZZ the 3rd dimension corresponds to the equation number.
% The same is true of DerivMat2.  For all other 2 and 1 dimensional
% matrices the rows correspond to equations.
%
% This function outputs the following:
%  X     - nx-by-1 column vector containing the value of the endogenous
%          state variables for next period

% Use quadratic approximation if no value is given for Pord
if nargin < 10
    Pord = 2;
end
if Pord ~= 1
    Pord = 2;
end

% Find the number of each kind of state variable
%  Using Xm find nx
[narg,ncol] = size(Xm);
nx = narg;
if narg > 1 && ncol > 1
    disp('Xm must be a column vector')
    disp('you have input a 2-dimensional matrix')
elseif narg == 1 && ncol > 1
    disp('XM must be a column vector')
    disp('you have input row vector, which we will now transpose')
    Xm = XM';
    nx = ncol;
end
%  Using Z find nz
[narg,ncol]  = size(Z);
nz = narg;
if narg > 1 && ncol > 1
    disp('Z must be a column vector')
    disp('you have input a 2-dimensional matrix')
elseif narg == 1 && ncol > 1
    disp('Zmust be a column vector')
    disp('you have input row vector, which we will now transpose')
    Z = Z';
    nz = ncol;
end

%  Using GZ to find ny
[narg,~]  = size(GX);
ny = narg;

% Check conformity of input coefficient matrices
[d1,d2] = size(HX);
if d1 ~= nx || d2 ~= nx
    disp('dimensions of HX incorrect')
end
[d1,d2] = size(HZ);
if d1 ~= nx || d2 ~= nz
    disp('dimensions of HZ incorrect')
end
[d1,d2,d3] = size(HXX);
if d1 ~= nx || d2 ~= nx || d3 ~= nx
    disp('dimensions of HXX incorrect')
end
[d1,d2,d3] = size(HXZ);
if d1 ~= nx || d2 ~= nz || d3 ~= nx
    disp('dimensions of HXZ incorrect')
end
[d1,d2,d3] = size(HZZ);
if d1 ~= nz || d2 ~= nz || d3 ~= nx
    disp('dimensions of HZZ incorrect')
end
[d1,d2] = size(H0);
if d1 ~= nx || d2 ~= 1
    disp('dimensions of H0 incorrect')
end
[d1,d2] = size(Hvv);
if d1 ~= nx || d2 ~= 1
    disp('dimensions of Hvv incorrect')
end

if ny>0
    % Check conformity of input coefficient matrices
    [~,d2] = size(GX);
    if d2 ~= nx
        disp('dimensions of HX incorrect')
    end
    [d1,d2] = size(GZ);
    if d1 ~= ny || d2 ~= nz
        disp('dimensions of HZ incorrect')
    end
    [d1,d2,d3] = size(GXX);
    if d1 ~= nx || d2 ~= nx || d3 ~= ny
        disp('dimensions of HXX incorrect')
    end
    [d1,d2,d3] = size(GXZ);
    if d1 ~= ny || d2 ~= nz || d3 ~= ny
        disp('dimensions of HXZ incorrect')
    end
    [d1,d2,d3] = size(GZZ);
    if d1 ~= nz || d2 ~= nz || d3 ~= ny
        disp('dimensions of HZZ incorrect')
    end
    [d1,d2] = size(G0);
    if d1 ~= ny || d2 ~= 1
        disp('dimensions of H0 incorrect')
    end
    [d1,d2] = size(Gvv);
    if d1 ~= ny || d2 ~= 1
        disp('dimensions of Hvv incorrect')
    end
end

% Construct HZX fro HXZ
HZX = permute(HXZ, [1 3 2]);

% Construct GZX fro GXZ
if ny > 0
    GZX = permute(GXZ, [1 3 2]);
else
    GZX = zeros(0,nx,nz);
end

% Permute 3-dimensional matrices for matrix multiplication below
HXXp = permute(HXX,[2 3 1]);
HXZp = permute(HXZ,[2 3 1]);
HZXp = permute(HZX,[2 3 1]);
HZZp = permute(HZZ,[2 3 1]);
GXXp = permute(GXX,[2 3 1]);
GXZp = permute(GXZ,[2 3 1]);
GZXp = permute(GZX,[2 3 1]);
GZZp = permute(GZZ,[2 3 1]);

% Generate data for next period, one equation at a time
X = zeros(nx,1);
Y = zeros(ny,1);
for i=1:nx
    % linear terms only
    X(i) = H0(i) + [HX(i,:) HZ(i,:)]*[Xm; Z];
    if ny>0
        Y(i) = G0(i) + [GX(i,:) GZ(i,:)]*[Xm; Z];
    end
    if Pord ~= 1
        % add quadriatic terms
        X(i) = X(i) + .5*[Xm; Z; v]' ...
            *[HXXp(:,:,i)  HXZp(:,:,i) 0;    ...
              HZXp(:,:,i)' HZZp(:,:,i) 0;    ...
              0           0          Hvv] ...
            *[Xm; Z; v];
        if ny>0
            Y(i) = Y(i) + .5*[Xm; Z; v]' ...
                *[GXXp(:,:,i)  GXZp(:,:,i) 0;    ...
                  GZXp(:,:,i)' GZZp(:,:,i) 0;    ...
                  0           0          Gvv] ...
                *[Xm; Z; v];            
        end
    end
end

end


