function x = parsolve(fnhandle,x0,tol,maxcount)

% parsolve was written by Ryan Decker, University of Maryland Economics.
% Version 2.0, November 2012
% fnhandle must be a valid function handle for the function of which you desire to find roots.
% x0 is the starting value guess.
% tol is an optional argument for the solve tolerance; default is 1e-10.
% maxcount is an optional argument for timing out the solver; default is
% 1000.

if nargin<4
    maxcount = 1000;
end
if nargin<3
    tol = 1e-10;
end

count = 0;
gap = 10;
m = length(x0);
argsz = size(x0);
x = x0;

eps = 1e-8;
epsvec = [eye(m)*eps,zeros(m,1)];
df = zeros(m,m+1);
J = zeros(m,m);
dft = zeros(m,1);

while gap>tol & count<=maxcount
    count = count+1;
    
    parfor i = 1:(m+1)
		if argsz(1)>=argsz(2)
			epstemp = epsvec(:,i);
		else
			epstemp = epsvec(:,i)';
		end
		
        temp = feval(fnhandle,x+epstemp);

        dft = zeros(m,1)

        for j = 1:m
            dft(j,1) = temp(j);
        end
        df(:,i) = dft;
    end
    
    for i=1:m
        J(:,i) = (df(:,i)-df(:,m+1))/eps;
    end
    
    if argsz(1)>=argsz(2)
        x = x - J\df(:,m+1);
    else
        x = x - (J\df(:,m+1))';
    end
    
    gap = sum(df(:,m+1).^2);

end

if count<maxcount
    disp('parsolve completed because the function value is below tolerance.')
else
    disp('parsolve stopped because iterations exceeded the maximum specified.')
end



end