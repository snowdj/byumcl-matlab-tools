function [ fy, fx, fyy, fxy, fxx, fyyy, fxyy, fxxy, fxxx ]...
    = Bnumdiff3( funcname, y0, x0, ep )
% Version 1.0
% Written by Kerk Phillips, March 31, 2014.
%
% Bbumdiff3 - This function returns first, second, and third numerical 
% derivatives of a bivariate function.
%
%   Inputs are:
%   funcname - name for the function to be evaluated
%   y0 - value of y for evaluation point, y is FIRST input into function
%   x0 - value of x for evaluation point, x is second input into function
%   ep - value of epsilon for numerical diffrentiation
% 
%   Outputs are:
%   fy - first derivative with respect to y
%   fx - first derivative with respect to x
%   fyy - second derivative with respect to y
%   fxy - second derivative with respect to y & x
%   fxx - second derivative with respect to x
%   fyyy - third derivative with respect to y
%   fxyy - third derivative with respect to y tiwce & x once
%   fxxy - third derivative with respect to y once & x twice
%   fxxx - third derivative with respect to x

% evaluate the function at various points
fA = funcname( y0+2*ep, x0-ep);
fB = funcname( y0+2*ep, x0);
fC = funcname( y0+2*ep, x0+ep);
fD = funcname( y0+ep, x0-2*ep);
fE = funcname( y0+ep, x0-ep);
fF = funcname( y0+ep, x0);
fG = funcname( y0+ep, x0+ep);
fH = funcname( y0+ep, x0+2*ep);
fI = funcname( y0, x0-2*ep);
fJ = funcname( y0, x0-ep);
fK = funcname( y0, x0);
fL = funcname( y0, x0+ep);
fM = funcname( y0, x0+2*ep);
fN = funcname( y0-ep, x0-2*ep);
fP = funcname( y0-ep, x0-ep);
fQ = funcname( y0-ep, x0);
fR = funcname( y0-ep, x0+ep);
fS = funcname( y0-ep, x0+2*ep);
fT = funcname( y0-2*ep, x0-ep);
fU = funcname( y0-2*ep, x0);
fV = funcname( y0-2*ep, x0+ep);

% take ratios to get numerical derivatives
fx = (fL-fJ)/(2*ep);
fy = (fF-fQ)/(2*ep);
fxx = (fL-2*fK+fJ)/(ep^2);
fxy = (fG-fE-fR+fP)/(ep^2);
fyy = (fF-2*fK+fQ)/(ep^2);
fxxx = (fM-2*fL+2*fJ-fI)/(2*ep^3);
fxxy = (fH-2*fF-fS+fD+2*fQ-fN)/(4*ep^3);
fxyy = (fA-2*fJ-fC+fT+2*fL-fV)/(4*ep^3);
fyyy = (fB-2*fF+2*fQ-fU)/(2*ep^3);

end