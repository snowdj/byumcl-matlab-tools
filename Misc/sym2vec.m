function A = sym2vec(B)
% converts a symmetric matrix into a vector
% B is a symmetric  matrix
% A is a row vectorization of it's upper triangular portion
A = [];
for k = 1:size(B,1)
    A = [A B(k,1:k)];
end

end