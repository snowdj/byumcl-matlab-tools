function X = XDDSolve( A, B, C, D, F )
% Version 1.0, written by Kerk Phillips, November 2013
% Implements the Gradient based iterative solutions for general linear
% matrix equations from Xie et al (2009).
%
% This function solves a Sylvester-transpose matrix equation of the
% following format:
%    sum_i=1,p A_i X B_i + sum_j=1_q C_j A^T D_j = F
% where the matrices A_i, B_i, C_j, D_j, and F are known square matrices
% and X is an unknown square matrix.
%
% This function takes the following inputs:
%  A is an r - by - m - by - p array of coefficients with the third 
%    dimension indexing i in the equation above.
%  B is an n - by - s - by - p array of coefficients with the third 
%    dimension indexing i in the equation above.
%  C is an r - by - n - by - q array of coefficients with the third 
%    dimension indexing j in the equation above.
%  D is an m - by - s - by - q array of coefficients with the third 
%    dimension indexing j in the equation above.
%  F is an r - by - s array of constants.
%
% This function takes the following inputs:
%  X is an m - by - n array that solves the equation.

% set display iterations option
dispiter = 0;

% set convergence  criterion
ccrit = .00000001;

% Find dimensions of matrices
[r,m,p] = size(A);
[n,s,~] = size(B);
[~,~,q] = size(C);

% Calculate convergence factor, mu, using ADD(2009) equation (14)
E1sum = 0;
E2sum = 0;
for i=1:p
    E1sum = E1sum + max(eig(A(:,:,i)*(A(:,:,i)')))* ...
      max(eig(B(:,:,i)'*(B(:,:,i))));
end
for i=1:q
    E2sum = E2sum + max(eig(C(:,:,i)*(C(:,:,i)')))* ...
      max(eig(D(:,:,i)'*(D(:,:,i))));
end
mu = 2*(E1sum + E2sum)^(-1);

% initial guess for X
X = ones(m,n)*.000001;
%X = [2.01 2.01; 2.01 2.01];
X = 100*randn(m,n);

% initialize distance measure
dist = 1;

% begin iteration loop
niter = 0;
while dist>ccrit
    % initialize additive terms, which are last terms in (12) and (13)
    addj = zeros(m,n,p);
    addp = zeros(m,n,p);

    % find square bracketed term in eqs (12) and (13)
    Q = F;
    for i=1:p
       Q = Q - A(:,:,i)*X*B(:,:,i);
    end
    for i=1:q
       Q = Q - C(:,:,i)*X'*D(:,:,i);
    end
    
    % calulate and add distances
    for j=1:p
        addj(:,:,j) = A(:,:,j)'*Q*B(:,:,j)';
    end
    for l=1:q
        addp(:,:,l) = D(:,:,l)'*Q'*C(:,:,l);
    end

    % update X according to equation (11)
    Xnew = X + mu*(sum(addj,3) + sum(addp,3))/(p+q);
    
    % calculate new distance measure
    dist = sum(sum(abs(X-Xnew),2),1);
    
    if dispiter
        msg = fprintf('iteration number: %6.0d, distance: %12.6f, Q: %12.6f %12.6f %12.6f %12.6f \n', niter, dist, Q);
        niter = niter + 1;
    end
    
    X = Xnew;
end

end

