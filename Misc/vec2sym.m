function B = vec2sym(A)
% converts a vector of the appropriate size into a symmetric matrix
%Let A be the given row vector with n*(n+1)/2 elements
[~,r] = size(A);
n = 0;
a = 1;
b = 1;
c = -2*r;
root1 = (-b+sqrt(b^2-4*a*c))/(2*a);
root2 = (-b+sqrt(b^2-4*a*c))/(2*a);
if mod(root1,1) == 0 && root1 == root2
    n = root1;
end

B = zeros(n,n);
cur = 1;
for k = 1:n
    len = k;
    B(1:len, k) = A(cur:cur+len-1);
    cur = cur + len;
end
B = triu(B,1)'+B;
% [p,q] = ndgrid(1:n,1:n);
% B = reshape(A(min((p-1).*(n-p/2)+q,(q-1).*(n-q/2)+p)),n,[]);

end