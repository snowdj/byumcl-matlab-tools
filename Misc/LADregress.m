function thetaout = LADregress(Y,X,thetaguess)
% Version 2.0, March 15th 2013, written by Kerk Phillips
% This function computes least absolute deviation regression coefficents
%  Inputs are:
%   Y:  a T-by-LADny matrix of dependent variables
%   X:  a T-by-LADnx matrix of independent variables
%   thetaguess:  a matrix of LADny * LADnx initial coefficient guesses
%  Output is:
%   thetaout:  a matrix of LADny * LADnx coefficient estimates
%  Global variables passed to LADcalc are:
%   LADY:  The Y matrix
%   LADX:  The X matrix
%   LADny:  The number of Y variables
%   LADnx:  The number of X variables

global LADY LADX LADnx LADny
[T,LADny] = size(Y);
[T,LADnx] = size(X);
LADY = Y;
LADX = X;

% set options for fminsearch
options = optimset('Display','off','MaxFunEvals',100000000,'MaxIter',200000,'TolFun',.000000000001,'TolX',.000000000001);
% seach over values of theta using fminsearch 
thetaout = fminsearch(@LADcalc,thetaguess,options);
end

function SAD = LADcalc(theta)
% This function computes least absolute deviation
%  Inputs are:
%   theta:  a matrix of LADny * LADnx coefficients
%  Output is:
%   SAD:  the sum of absolute deviations
%  Global variables passed from LADregress are:
%   LADY:  The Y matrix
%   LADX:  The X matrix
%   LADny:  The number of Y variables
%   LADnx:  The number of X variables

global LADY LADX LADnx LADny

% reshape theta to make it comformable
beta = reshape(theta,LADnx,LADny);
% calculate sum of absolute deviations
SAD = sum(sum(abs(LADY - LADX*beta))');
end