function [dGam, d2Gam, Gam0] = ...
    QuadApp_Deriv(funcname,param,theta,nx,ny,nz,logX,Pord)

% Version 1.0, written by Kerk Phillips, November 2013
% Implements Heer & Maussner (2007), chapter 11.3.1
%
% This function generates matrices dGam and d2Gam from the 
% log-lineaization or simple linearization of the equations in the function
% "funcname"
%
% This function takes the following inputs:
%  funcname - is the name of the function which generates a column vector 
%  param - is a vector of parameter values to be passed to funcname.
%  theta - is a (3nx+2ny+2nz)-by-1 column vector of inputs
%   There are seven sets of variables with the following designations:
%   Set 1 is X(t+1)
%   Set 2 is X(t)
%   Set 3 is X(t-1)
%   Set 4 is Y(t+1)
%   Set 5 is Y(t)
%   Set 6 is Z(t+1)
%   Set 7 is Z(t)
%   where X is the set of endogenous state variables,
%   Y is the set of "jump", costate or control variables,
%   and Z is the set of exogenous state variable
%  nx - is the number of elements in X
%  ny - is the number of elements in Y
%  nz - is the number of elements in Z
%  logX - is an indicator that determines if the X & Y variables are
%   log-linearized (true) or simply linearized (false).  Z variables are
%   always simply linearized.
%  POrd - is the order of deriviatives to take, 1 takes only 1st
%   derivatives and 2 takes 2nd derivatives as well
%
% This function creates the following outputs:
%  dGam - is an (nx+ny)-by-(3nx+2ny+2nz) matrix of first derivatives, 
%   with the rows corresponding to equations in the function and the 
%   columns corresponding to the 3nx+2ny+2nz elements of theta.
%  d2Gam - is an (3nx+2ny+2nz)-by-(3nx+2ny+2nz)-by-(nx+ny) array or 
%   tensor of second derivatives, with the rows corresponding to 
%   equations in the function and the other two dimensions corresponding 
%   to the 3nx+2ny+2nz elements of theta.
%  Gam0 - is an (nx+ny)-by-1 of function values when the input is theta.

% Use log-linearized X & Y if no value is specified for logX
if nargin < 5
    logX = true;
end

% Take second derivatives if no value is given for Pord
if nargin < 5
    logX = true;
    Pord = 2;
end
if Pord ~= 1
    Pord = 2;
end

% Value of the function at the linearization point 
%  This should be zero or very close to it when evaluating at the SS
Gam0 = funcname(theta,param);

% Check and report errors with nx & nz
[narg,ncol] = size(theta);
[neqn,~] = size(Gam0);
if narg ~= 3*nx+2*ny+2*nz
    % display error warning
end

if neqn ~= nx+ny
    disp('number of equation not equal to number of X variables')
    disp('continuing to get deriviatives, but check your code')
end

if narg > 1 && ncol > 1
    disp('input vector must be a column vector')
    disp('you have input a 2-dimensional matrix')
elseif narg == 1 && ncol > 1
    disp('input vector must be a column vector')
    disp('you have input row vector, which we will now transpose')
    theta = theta';
end

% Calculate the increments to use in calculating numerical derivatives
leng = 3*nx+2*ny+2*nz;
% Machine epsilon for double precision is named eps in MATLAB
epsnew = 1000000000*eps;
incr = sqrt(epsnew)*max(theta,sqrt(epsnew)*ones(leng,1));
thetap = theta + incr;
thetam = theta - incr;
dx = thetap - thetam;

% Create matrices of deviations matrices 
devp=repmat(theta,1,leng);
devm=repmat(theta,1,leng);

% Deviate columns and 3rd dimensions by adding and subtracting incr
for i=1:leng
  devp(i,i)=devp(i,i)+incr(i);
  devm(i,i)=devm(i,i)-incr(i);
end

% Get 1st Derivatives
% Calculate matrices of deviations for dynamic equations, bigp and bigm
%  rows correspond to equations
%  colums correspond to variables from "theta" vector being changed
%  note output of funcname must be a column vector

% Initialize the function calls when inputs are deviated
bigp = zeros(nx+ny,leng);
bigm = zeros(nx+ny,leng);

% Call and evaluate functions with deviated inputs
%  We add a 1 to FO for the log-linearization case, since the functions are 
%  to be written to evaluate to 0, not 1.
for i = 1:leng
    if logX 
        if i<3*nx+1
            bigp(:,i) = theta(i)*(funcname(devp(:,i),param))./(1+Gam0);
            bigm(:,i) = theta(i)*(funcname(devm(:,i),param))./(1+Gam0);
        else
            bigp(:,i) = (funcname(devp(:,i),param))./(1+Gam0);
            bigm(:,i) = (funcname(devm(:,i),param))./(1+Gam0);
        end
    else
        bigp(:,i) = funcname(devp(:,i),param);
        bigm(:,i) = funcname(devm(:,i),param);
    end
end

% Calclate the derivatives using the central difference formula
dGam = (bigp-bigm)./repmat(dx',[nx+ny 1]);


% Get 2nd Derivatives, if requested
if Pord == 2
    % Create matrices of deviations matrices 
    devpp=repmat(theta,[1,leng,leng]);
    devpm=repmat(theta,[1,leng,leng]);
    devmp=repmat(theta,[1,leng,leng]);
    devmm=repmat(theta,[1,leng,leng]);

    % Deviate columns and 3rd dimensions by adding and subtracting incr
    for i=1:leng
      devpp(i,i,:)=devpp(i,i,:)+incr(i);
      devpm(i,i,:)=devpm(i,i,:)-incr(i);  
      devmp(i,i,:)=devmp(i,i,:)+incr(i);
      devmm(i,i,:)=devmm(i,i,:)-incr(i);
    end
    for i=1:leng
      devpp(i,:,i)=devpp(i,:,i)+incr(i);
      devpm(i,:,i)=devpm(i,:,i)+incr(i);  
      devmp(i,:,i)=devmp(i,:,i)-incr(i);
      devmm(i,:,i)=devmm(i,:,i)-incr(i);
    end

    % Calculate matrices of deviations for dynamic equations, bigpp, bigpm, 
    % bigmp and bigmm
    %  rows correspond to equations
    %  colums & 3rd dimension correspond to variables from "theta" vector 
    %  being changed
    %  note output of funcname must be a column vector

    % Initialize the function calls when inputs are deviated
    bigpp = zeros(nx+ny,leng,leng);
    bigpm = zeros(nx+ny,leng,leng);
    bigmp = zeros(nx+ny,leng,leng);
    bigmm = zeros(nx+ny,leng,leng);
    % Get value of Gam0 squared for cases
    Gam0sq = zeros(nx+ny,1);

    for i=1:nx+ny
        Gam0sq(i) = 1 + Gam0(i)^2;
    end

    % Call and evaluate functions with deviated inputs
    %  We add a 1 to FO for the log-linearization case, since the functions
    %  are to be written to evaluate to 0, not 1.

    for i = 1:leng
        for j = 1:leng
            bigpp(:,i,j) = funcname(devpp(:,i,j),param);
            bigpm(:,i,j) = funcname(devpm(:,i,j),param);
            bigmp(:,i,j) = funcname(devmp(:,i,j),param);
            bigmm(:,i,j) = funcname(devmm(:,i,j),param);
        end
    end

    d2x = zeros(nx+ny,leng,leng);
    for k=1:nx+ny
        for i=1:leng
            for j=1:leng
                d2x(k,i,j) = dx(i)*dx(j);
            end
        end
    end

    d2Gam = (bigpp-bigpm-bigmp+bigmm)./(d2x);

    % Adjust for long-linearization, if needed
    if logX
        for i = 1:leng
            for j = 1:leng
                if i<3*nx+2*ny+1 && j<3*nx+2*ny+1
                   d2Gam(:,i,j) = theta(i)*theta(j)*d2Gam(:,i,j);
                elseif i<3*nx+2*ny+1
                   d2Gam(:,i,j) = theta(i)*d2Gam(:,i,j);
                elseif j<3*nx+2*ny+1
                   d2Gam(:,i,j) = theta(j)*d2Gam(:,i,j);
                end
            d2Gam(:,i,j) = d2Gam(:,i,j)./(1+Gam0) ...
                             - dGam(:,i).*dGam(:,j);
            end
        end
    end
end


end